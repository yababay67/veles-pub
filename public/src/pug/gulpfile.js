var gulp    = require('gulp');
var pug     = require('gulp-pug');

gulp.task('pug', function() {
    var YOUR_LOCALS = {}
    gulp.src('index.pug')
        .pipe(pug({locals: YOUR_LOCALS}))
        .pipe(gulp.dest('../../'))
})

gulp.task('print', function() {
    var YOUR_LOCALS = {}
    gulp.src('print.pug')
        .pipe(pug({locals: YOUR_LOCALS}))
        .pipe(gulp.dest('../../'))
})

gulp.task('default', ['pug', 'print'])
