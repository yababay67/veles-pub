define ['barnlib', 'shortid'], (Barn, shortid)->
    Barn.prototype.add = (dict, id, val)->
        if not val
            val = id
            id = shortid.gen()
        @set id, val
        @sadd dict, id
        id
    Barn.prototype.rem = (dict, id)->
        @del id
        @srem dict, id
    barn = new Barn localStorage
    barn
