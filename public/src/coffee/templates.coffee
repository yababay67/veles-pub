define ['vue', 'barn', 'validators'], (Vue, barn, validators)->

    Vue.component 'chart-footer-radio',
        template: '#template-chart-footer-radio'
        props: ['paramid']
        computed:
            title: ->
                return 'всё' unless @paramid isnt 'all'
                (barn.get(@paramid) or @paramid).toLowerCase()
        methods:
            showLines: ->
                alert 123

    Vue.component 'chart-footer',
        template: '#template-chart-footer'
        props: ['chartid']

    Vue.component 'mdl-dialog',
        template: '#template-mdl-dialog'
        props: ['title', 'dlgid']

    Vue.component 'card-10-of-12-holder',
        template: '#template-card-10-of-12-holder'
        props: ['title', 'cardid']

    Vue.component 'card-8-of-12-holder',
        template: '#template-card-8-of-12-holder'
        props: ['title', 'cardid']

    Vue.component 'card-6-of-12-holder',
        template: '#template-card-6-of-12-holder'
        props: ['title', 'cardid']

    Vue.component 'card-4-of-12-holder',
        template: '#template-card-4-of-12-holder'
        props: ['title', 'cardid']

    Vue.component 'card-3-of-12-holder',
        template: '#template-card-3-of-12-holder'
        props: ['title', 'cardid']

    Vue.component 'card-6-of-12',
        template: '#template-card-6-of-12'
        props: ['title']

    Vue.component 'card-8-of-12',
        template: '#template-card-8-of-12'
        props: ['title']

    Vue.component 'card-10-of-12',
        template: '#template-card-10-of-12'
        props: ['title']

    Vue.component 'card-4-of-12',
        template: '#template-card-4-of-12'
        props: ['title']

    Vue.component 'card-3-of-12',
        template: '#template-card-3-of-12'
        props: ['title']

    Vue.component 'row-1-of-3',
        template: '#template-row-1-of-3'
        props: ['title', 'cardid']

    Vue.component 'row-1-of-1-6',
        template: '#template-row-1-of-1-6'
        props: ['title', 'cardid']

    Vue.component 'row-1-of-1-8',
        template: '#template-row-1-of-1-8'
        props: ['title', 'cardid']

    Vue.component 'row-1-of-1-10',
        template: '#template-row-1-of-1-10'
        props: ['title', 'cardid']

    Vue.component 'row-2-of-4',
        template: '#template-row-2-of-4'
        props: ['title1', 'title2', 'cardid1', 'cardid2']

    Vue.component 'row-3-of-3',
        template: '#template-row-3-of-3'
        props: ['title1', 'title2', 'title3', 'cardid1', 'cardid2', 'cardid3']

    Vue.component 'row-4-of-4',
        template: '#template-row-4-of-4-cards'
        props: ['title1', 'title2', 'title3', 'title4', 'cardid1', 'cardid2', 'cardid3', 'cardid4']

    Vue.component 'popup-menu',
        template: '#template-popup-menu'
        props: ['dict', 'label']
        methods:
            getItems: ->
                arr = barn.smembers @dict.replace('-choice', '')
                if not arr then return []
                arr.map (el)->
                    {id: el, name: barn.get el}

    Vue.component 'popup-item',
        template: '#template-popup-item'
        props: ['item', 'name', 'dict']
        methods:
            selectIt: ->
                isFound = false
                dict = @dict
                id = @itemid
                nm = @name
                $form = $(@$el).closest('.mdl-layout').find('#report-fields')
                ['', '-1', '-2', '-3', '-4', '-5'].forEach (el)->
                    return unless not isFound
                    arr = $form.find "[name=of-#{dict}#{el}]"
                    return unless arr.length
                    txt = $(arr[0]).val().trim()
                    return unless not txt or not el
                    $(arr[0]).val nm
                    isFound = true
                null

    Vue.component 'dict',
        template: '#template-dictionary'
        props: ['dictid', 'items', 'deletable', 'addable', 'dictclass', 'reverse', 'clickable', 'editable', 'defaults']
        methods:
            getValue: (id)->
                barn.get(id) or @getDefault(id) or id
            getItems: ->
                return @items.split(' ') unless not @items
                arr = barn.smembers(@dictid) or []
                return arr unless @reverse is 'true'
                arr.reverse()
            getDefault: (id)->
                return null unless typeof @defaults is 'object'
                @defaults[id]

        mounted: ->
            return unless @items and @defaults
            keys = @items.split ' '
            vals = @defaults.split /;\ */
            @defaults = {}
            dfl = @defaults
            keys.forEach (el, i)->
                val = vals[i]
                dfl[el] = val
                barn.set el, val unless not val

    Vue.component 'dict-item',
        template: '#template-dictionary-item'
        props: ['itemid', 'itemvalue', 'deletable', 'editable', 'clickable']
        mounted: ->
            $(@.$el).on "focusout", "input", ()->
                return unless not $(@).prop 'disabled'
                key = $ @
                    .prop 'disabled', true
                    .removeClass 'cst-input-enabled'
                    .attr 'name'
                barn.set key, $(@).val()
        methods:
            clickIt: ->
                null
            editIt: ->
                $(@.$el).find("input")
                    .prop 'disabled', false
                    .addClass 'cst-input-enabled'
                    .focus()
            saveIt: ->
                val = $(@.$el).find "input"
                    .prop 'disabled', true
                    .removeClass 'cst-input-enabled'
                    .val()
                isValid = validators[@$parent.dictid]
                if isValid and isValid val then barn.set @itemid, val; return
                barn.set @itemid, val
            deleteIt: ->
                return unless confirm 'Действительно удалить?'
                barn.rem @$parent.dictid, @itemid
                $(@$el).remove()

    Vue.component 'dict-adder',
        template: '#template-dictionary-adder'
        props: ['dictid', 'editable', 'deletable', 'checkable']
        methods:
            addIt: ->
                isValid = validators[@dictid]
                $inp = $(@$el).find('input')
                val = $inp.val().trim()
                return unless (typeof isValid is 'function' and isValid val) or not isValid
                $inp.val ''
                id = barn.add @dictid, val
                $el = $ "<dict-item itemid=#{id}>"
                    .attr 'itemvalue', val
                    .attr 'editable',  @editable  is 'true'
                    .attr 'deletable', @deletable is 'true'
                    .attr 'chekable',  @checkable is 'true'
                    .appendTo "##{@dictid} ul"
                new Vue
                    el: $el[0]

    Vue.component 'form-field',
        template: '#template-form-field'
        props: ['input', 'label', 'wide', 'type', 'inputvalue', 'disabled']
        methods:
            setFileName: ->
                $('#uploadable-file-name').val $('#uploadable-file').val()
    null
