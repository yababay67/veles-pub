define ['barn', 'moment', 'vue', 'reportchart', 'jquery'], (barn, moment, Vue)->
    hash = window.location.hash
    if not hash or not /^\#\d\d\d\d\-/.test hash then alert 'Не указан идентификатор отчета'; return
    docnum = prompt 'Введите номер документа'
    docnum = ' _____ ' unless docnum
    date   = ' __________________ '
    moment.locale 'ru'
    if confirm 'Использовать сегодняшнюю дату?' then date = moment().format('LL') #unless not confirm 'Использовать сегодняшнюю дату?'
    Vue.component "top-grid",
        template: "#top-grid"
        props: ['customer', 'begin', 'duration', 'date', 'docnum',
                'staff1', 'staff2', 'staff3', 'supervisor1', 'supervisor2', 'supervisor3',
                'field', 'bush', 'well', 'volume', 'density', 'welltype', 'worktype']
        computed:
            performer: ->
                perf = ''
                param = barn.get "performers-name"
                if param then perf += param
                param = barn.get "performers-inn"
                if param then perf += " ИНН: #{param},"
                param = barn.get "performers-address"
                if param then perf += " адрес: #{param},"
                param = barn.get "performers-phone"
                if param then perf += " тел.: #{param},"
                param = barn.get "performers-email"
                if param then perf += " e-mail.: #{param},"
                perf.replace /\,$/, ''
    hash = hash.substring 1
    cnt  = 0
    obj  = JSON.parse barn.get("#{hash}:form")
    $header  = $ 'header'
    after = (svg, data, x)->
        fieldsAvg = (_arr, fld)-> Math.round((_arr.map (el)-> el[fld]).reduce((a, b)-> a + b) / _arr.length * 100) / 100
        volume = 0
        data.forEach (el)->
            if el.volume > volume then volume = el.volume
        density = fieldsAvg data, 'density'
        dur  = moment.duration(moment(data[0].date).diff data[data.length - 1].date)
        durH = Math.abs dur.hours()
        durM = Math.abs(dur.minutes()) % 60
        $topGrid = $ "<top-grid>"
            .attr 'docnum',      docnum
            .attr 'date',        date
            .attr 'customer',    obj.customer
            .attr 'staff1',      obj['of-staff-1']
            .attr 'staff2',      obj['of-staff-2']
            .attr 'staff3',      obj['of-staff-3']
            .attr 'supervisor1', obj['of-supervisors-1']
            .attr 'supervisor2', obj['of-supervisors-2']
            .attr 'supervisor3', obj['of-supervisors-3']
            .attr 'field',       obj['of-fields'].replace /\(\d+\)/, ''
            .attr 'bush',        obj['bush']
            .attr 'well',        obj['well']
            .attr 'welltype',    obj['welltype']
            .attr 'worktype',    obj['worktype']
            .attr 'volume',      volume
            .attr 'density',     density
            .attr 'begin',       moment(data[0].date).format('lll')
            .attr 'duration',    "#{durH} ч. #{durM} мин."
            .appendTo $header
        new Vue el: $topGrid[0]
        return unless obj.stages
        stages = obj.stages.split ';'
        return unless stages and stages.length
        stages.forEach (el)->
            arr = el.split '|'
            return unless arr.length is 3
            d1 = moment arr[1]
            d2 = moment arr[2]
            if moment(d2).isBefore d1 then t = d2; d2 = d1; d1 = t
            diff = d2.diff d1
            durM = Math.round moment.duration(diff).asMinutes()
            durH = Math.round moment.duration(diff).asHours()
            if durH then dur = "#{durH} ч. "
            else durH = ''
            dur     = "#{durH}#{durM} мин."
            bfr = true
            aft = false
            farr = data.filter (el)->
                mom = moment el.date
                mom.isAfter(d1) and mom.isBefore(d2)
            volume1 = farr[0].volume
            volume2 = farr[farr.length - 1].volume
            # fieldsAvg = (fld)-> Math.round((farr.map (el)-> el[fld]).reduce((a, b)-> a + b) / farr.length * 100) / 100
            consAvg   = fieldsAvg farr, 'consumption'
            densAvg   = fieldsAvg farr, 'density'
            pressAvg  = fieldsAvg farr, 'pressure1'
            $table = $ '#stages tbody'

            $ """
                <tr>
                    <td align='center'>#{++cnt}.</td>
                    <td>#{arr[0]}</td>
                    <td align='right'>#{dur}</td>
                    <td align='right'>#{(volume2 - volume1)}</td>
                    <td align='right'>#{consAvg}</td>
                    <td align='right'>#{densAvg}</td>
                    <td align='right'>#{pressAvg}</td>
                </tr>
            """
                .appendTo $table
    el = $('#svg-holder')[0]
    require('reportchart') el, hash, 1100, 700, after
    null
