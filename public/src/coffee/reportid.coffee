FIELD_ZEROS = 2
BUSH_ZEROS  = 6
WELL_ZEROS  = 6

leadingZeros = (num, count)->
    while num.length < count
        num = "0#{num}"
    num

getReportId = (date, field, bush, well)->
        "#{date.replace(' ', 'T').replace ':', '-'}-#{leadingZeros field, FIELD_ZEROS}-#{leadingZeros bush, BUSH_ZEROS}-#{leadingZeros well, WELL_ZEROS}"

if typeof window isnt 'undefined'
    define ->
        getReportId

else
    module.exports = getReportId
