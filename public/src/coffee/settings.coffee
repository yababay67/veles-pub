define ['tabloader', 'vue', 'module', 'barn'], (tabLoader, Vue, module, barn)->
    fields = ['name', 'phone', 'email', 'address', 'inn']
    id = module.id
    Vue.component "tab-body-#{id}",
        template: "#template-tab-#{id}"
        methods:
            savePerformersData: ->
                #alert 123
                null
        mounted: ->
            fields.forEach (el)->
                key = "performers-#{el}"
                val = barn.get key
                return unless val
                $(@$el).find("[name=#{key}]").val val

    $tab = tabLoader id
    form = document.forms['performers-form']
    form.onsubmit = (e)->
        fields.forEach (el)->
            key = "performers-#{el}"
            inp = form[key]
            return unless inp and inp.value
            barn.set key, inp.value
        logo = form['performers-logo'].value
        alert 'no logo'; e.preventDefault(); return false unless logo and /(\.png|\.jpg|\.jpeg)$/.test logo
        true
    buildDict = (id, markup)->
        $card = $tab.find "##{id}-dict .mdl-card__supporting-text"
        return unless $card.length
        $markup = $ markup
            .attr('dictid', id)
            .attr('dictclass', 'cst-parameters-list')
        $card = $card
            .append $markup
            .parent()
        if $markup.attr('addable') is 'true'
            $ "<dict-adder dictid=#{id}>"
                .attr 'editable',  'true' is $markup.attr 'editable'
                .attr 'deletable', 'true' is $markup.attr 'deletable'
                .attr 'checkable', 'true' is $markup.attr 'checkable'
                .appendTo $card

        new Vue
            el: $card[0]
    buildDict id, markup for id, markup of {
        parameters:      '''
            <dict
                deletable="false" addable="false"  editable="true"
                items="volume consumption pressure1 pressure2 pressure3 temperature density"
                defaults="Объем; Расход; Давление 1; Давление 2; Давление 3; Температура; Плотность"
            >
        '''
    }
    null
