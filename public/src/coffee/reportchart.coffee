define ['d3v4', 'shortid', 'vue', 'barn'], (d3, shortid, Vue, barn)->
    (el, url, width = 960, height = 500, after)->
        $(el).empty()
        margin = {top: 20, right: 60, bottom: 30, left: 50}
        width  = width  - margin.left - margin.right
        height = height - margin.top - margin.bottom

        parseTimeDMY = d3.timeParse  "%d.%m.%Y %H:%M:%S"
        parseTimeYMD = d3.timeParse  "%Y-%m-%d %H:%M:%S"
        formatTime   = d3.timeFormat "%Y-%m-%d %H:%M:%S"
        bisectDate   = d3.bisector((d)-> d.date).left

        x  = d3.scaleTime().range([0, width])
        y0 = d3.scaleLinear().range([height, 0])
        y1 = d3.scaleLinear().range([height, 0])
        y2 = d3.scaleLinear().range([height, 0])
        y3 = d3.scaleLinear().range([height, 0])
        y4 = d3.scaleLinear().range([height, 0])
        y5 = d3.scaleLinear().range([height, 0])

        volumeLine = d3.line()
            .x((d)-> x(d.date))
            .y((d)-> y0(d.volume))

        consumptionLine = d3.line()
            .x((d)-> x(d.date))
            .y((d)-> y1(d.consumption))

        temperatureLine = d3.line()
            .x((d)-> x(d.date))
            .y((d)-> y2(d.temperature))

        pressure1Line = d3.line()
            .x((d)-> x(d.date))
            .y((d)-> y3(d.pressure1))

        pressure2Line = d3.line()
            .x((d)-> x(d.date))
            .y((d)-> y4(d.pressure2))

        densityLine = d3.line()
            .x((d)-> x(d.date))
            .y((d)-> y5(d.density))

        svg = d3.select(el).append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")")

        drawChart = (data)->
            normField = (k, f, d)->
                f = f.replace ',', '.' unless typeof f isnt 'string'
                if isNaN f then f = 0
                d[k] = +f
            data.forEach (d)->
                return unless typeof d.date is 'string'
                d.date = parseTimeDMY d.date
                normField k, v, d for k, v of d when k isnt 'date'

            x.domain(d3.extent(data,   (d)-> d.date))
            y0.domain([0, d3.max(data, (d)-> Math.max(d.volume))])
            y1.domain([0, d3.max(data, (d)-> Math.max(d.consumption))])

            svg.append("g")
                .attr("transform", "translate(0," + height + ")")
                .call(d3.axisBottom(x))

            svg.append("g")
                .attr("class", "axisSteelBlue")
                .call(d3.axisLeft(y0))

            svg.append("g")
                .attr("class", "axisRed")
                .attr("transform", "translate( " + width + ", 0 )")
                .call(d3.axisRight(y1))

            svg.append("path")
                .data([data])
                .attr("class", "svg-volume")
                .attr("d", volumeLine)

            svg.append("path")
                .data([data])
                .attr("class", "svg-consumption")
                .attr("d", consumptionLine)

            svg.append("path")
                .data([data])
                .attr("class", "svg-temperature")
                .attr("d", temperatureLine)

            svg.append("path")
                .data([data])
                .attr("class", "svg-pressure1")
                .attr("d", pressure1Line)

            svg.append("path")
                .data([data])
                .attr("class", "svg-pressure2")
                .attr("d", pressure2Line)

            svg.append("path")
                .data([data])
                .attr("class", "svg-density")
                .attr("d", densityLine)

            return unless typeof url is 'string'
            obj = JSON.parse barn.get("#{url}:form")
            if obj and obj.stages and obj.stages.split(';').length
                stages = obj.stages
                stages.split(';').forEach (el)->
                    id = shortid.gen()
                    arr = el.split '|'
                    return unless arr.length is 3
                    date1 = parseTimeYMD arr[1]
                    date2 = parseTimeYMD arr[2]
                    if date2 < date1 then t = date2; date2 = date1; date1 = t
                    setTimeout ->
                            _x1 = Math.round x(date1)
                            _x2 = Math.round x(date2)
                            svg.append("rect")
                                .attr("class", "svg-selected-zone")
                                .attr("width",  (_x2 - _x1))
                                .attr("height", height)
                                .attr("id", id)
                                .attr("transform", "translate( " + _x1 + ", 0 )")
                            svg.append("text")
                                .attr("class", "svg-zone-label")
                                .attr("x", _x2 - 15)
                                .attr("y", 10)
                                .attr("style", "writing-mode: tb; glyph-orientation-vertical: 0;")
                                .text arr[0]
                        , 1000
            if not /print\.html/.test window.location.pathname
                chartFooter = $ "<chart-footer chartid=\'#{shortid.gen()}\'>"
                    .insertAfter svg.node().parentNode
                new Vue el: chartFooter[0]
            return unless typeof after is 'function'
            after svg, data, x
            null
        if typeof url isnt 'string' then drawChart(url); return
        d3.csv "/data/#{url}.csv?r=#{Math.random()}", (err, dat)->
            if err then throw err
            drawChart dat
        null
