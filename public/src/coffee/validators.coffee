define ->
    fields: (val)->
        if not isNaN val
            if +val < 1000 then return true
            else alert 'Номер месторождения должен быть меньше 1000.'; return false
        arr = /.+\((\d+)\)$/.exec val
        if arr and arr.length > 0 and not isNaN arr[1] then return true
        alert 'Укажите номер в скобках. Пример: "Самотлор (123)".'
        false
    bushes: (val)->
        if not val or isNaN val then alert 'Поле "Куст" должно содержать число'; return false
        true
    wells: (val)->
        if not val or isNaN val then alert 'Поле "Скважина" должно содержать число'; return false
        true
    isoDate: (val)->
        if not val or not /^\d\d\d\d\-\d\d\-\d\d\ \d\d\:\d\d$/.test val
            alert 'Некорректно указана дата. Формат ГГГГ-ММ-ДД ЧЧ:ММ.'; return false
        true
