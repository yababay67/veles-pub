define ['jquery', 'vue'], ($, Vue)->
    (id)->
        $tab = $ "#tab-#{id}"
        $holder = $tab.find '.cst-hidden'
        $holder.append "<tab-body-#{id}>"
        new Vue
            el: $holder[0]
        $holder = $tab.find '.cst-hidden'
        setTimeout ->
                $tab.find('.cst-progress-wrapper').remove()
                $holder.removeClass 'cst-hidden'
                null
            , 1000
        $tab
