define ['tabloader', 'vue', 'module', 'barn'], (tabLoader, Vue, module, barn)->
    id = module.id
    Vue.component "tab-body-#{id}",
        template: "#template-tab-#{id}"
    $tab = tabLoader id
    buildDict = (id, markup)->
        $card = $tab.find "##{id}-dict .mdl-card__supporting-text"
        return unless $card.length
        $markup = $ markup
            .attr('dictid', id)
            .attr('dictclass', 'cst-dict-list')
        $card = $card
            .append $markup
            .parent()
        if $markup.attr('addable') is 'true'
            $ "<dict-adder dictid=#{id}>"
                .attr 'editable',  'true' is $markup.attr 'editable'
                .attr 'deletable', 'true' is $markup.attr 'deletable'
                .attr 'checkable', 'true' is $markup.attr 'checkable'
                .appendTo $card

        new Vue
            el: $card[0]
    buildDict id, markup for id, markup of {
        fields:      '<dict deletable="true" addable="true"  editable="true">'
        devices:     '<dict deletable="true" addable="false" editable="true">'
        staff:       '<dict deletable="true" addable="true"  editable="true">'
        customers:   '<dict deletable="true" addable="true"  editable="true">'
        supervisors: '<dict deletable="true" addable="true"  editable="true">'
        welltypes:   '<dict deletable="true" addable="true"  editable="true">'
        worktypes:   '<dict deletable="true" addable="true"  editable="true">'
    }
    null
