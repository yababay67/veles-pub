require.config
	waitSeconds: 30
	baseUrl:    'js'
	paths:
        d3v4:     'vendor/d3.v4.min'
        d3v3:     'vendor/d3.min'
        c3:       'vendor/c3.min'
        ws:       'vendor/reconnecting-websocket.min'
        barnlib:  'vendor/barn.min'
        shortid:  'vendor/js-shortid.min'
        vue:      'vendor/vue.min'
        jquery:   'vendor/jquery.min'
        dialog:   'vendor/dialog-polyfill'
        validate: 'vendor/validate.min'
        moment:   'vendor/moment-with-locales.min'
    shim:
        c3:         {deps: ['d3v3']}
        tabloader:  {deps: ['templates']}
        upload:     {deps: ['validate']}

window.onload = ->
    require ['upload']
    alert 'Обновлено 11 апреля в 16:25'
