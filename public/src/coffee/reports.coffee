define ['tabloader', 'vue', 'module', 'barn', 'reportchart', 'd3v4', 'reportid', 'validators', 'shortid', 'dialog', 'moment'], (tabLoader, Vue, module, barn, chart, d3, getReportId, validators, shortid, dialog, moment)->
    id = module.id
    $currentNavItem = null
    $chartDialog = $('.cst-chart-dialog')
    chartDialog  = $chartDialog[0]
    dialog.registerDialog chartDialog
    $(chartDialog).find 'button'
        .click ->
            chartDialog.close()
    parseTimeYMD = d3.timeParse  "%Y-%m-%d %H:%M:%S"
    Vue.component "stage",
        template: "#template-report-form-stage"
        props: ['title', 'begin', 'end', 'stageid', 'chartData']
        methods:
            deleteRow: ->
                return unless confirm 'Действительно удалить?'
                d3.select("#tab-reports svg [id=#{@stageid}").remove()
                $(@$el).remove()
            viewRow: ->
                if not @chartData
                    d1 = moment @begin
                    d2 = moment @end
                    if moment(d2).isBefore d1 then t = d2; d2 = d1; d1 = t
                    $holder = $ '#tab-reports #stage-holder'
                    @chartData = $holder.data('chart-data').filter (el)->
                        mom = moment el.date
                        mom.isAfter(d1) and mom.isBefore(d2)
                $svgHolder = $chartDialog.find '.chart-holder'
                require('reportchart') $svgHolder[0], @chartData
                $chartDialog.find('h6').text "#{@title} (#{@begin.replace /\d\d\d\d-\d\d-\d\d /, ''} - #{@end.replace /\d\d\d\d-\d\d-\d\d /, ''})"
                chartDialog.showModal()
        mounted: ->
            $holder = $ '#tab-reports #stage-holder'
            @chartData = $holder.data 'chart-data'
    Vue.component "tab-body-#{id}",
        template: "#template-tab-#{id}"
    Vue.component "report-form",
        template: "#template-report-form"
        props: ['date', 'field', 'bush', 'well', 'reportid', 'title']
        mounted: ->
            $title = $('[name=stage-title]')
                .prop 'disabled', true
                .attr 'id', 'stage-title'
        methods:
            printReport: ->
                @saveReport true
                window.open "/print.html##{@reportid}", '_blank'
            saveReport: (mute)->
                form = document.forms['report-form']
                if not validators['fields'](form['of-fields'].value) then return
                if not validators['bushes'](form['bush'].value)      then return
                if not validators['wells'](form['well'].value)       then return
                if not validators['isoDate'](form['date'].value)     then return
                obj = {}
                stages = ''
                $('#tab-reports #stage-holder tr').each ->
                    n = 0
                    $(@).find('td').each ->
                        return unless ++n < 4
                        delim = '|'
                        if n is 3 then delim = ';'
                        stages += "#{$(@).text()}#{delim}"
                obj[$(inp).attr 'name'] = inp.value for key, inp of form when inp and inp.value
                if stages then obj.stages = stages
                fld = obj['of-fields']
                if isNaN fld
                    arr = /.+\((\d+)\)$/.exec fld
                    fld = arr[1]
                reportName = getReportId obj.date, fld, obj.bush, obj.well
                    .replace("#{obj.date}-", "#{obj.date} ")
                    .replace('T', ' ')
                barn.set @reportid, reportName
                $currentNavItem.val reportName
                barn.set "#{@reportid}:form", JSON.stringify obj
                alert 'Данные сохранены.' unless mute
            getName: ->
                barn.get @reportid
            addStage: ->
                $title = $ '#stage-title'
                $begin = $ '#stage-begin'
                $end   = $ '#stage-end'
                $id    = $ '#stage-id'
                $add   = $ '#stage-adder'
                title  = $title.val().trim()
                begin  = $begin.text()
                end    = $end.text()
                if parseTimeYMD(begin) > parseTimeYMD(end)
                    t = begin
                    begin = end
                    end = t
                id     = $id.val()
                if not title then alert 'Укажите название этапа.'; return
                if /[\;\|]/.test title then alert 'Название этапа не должно содержать символов ";" и "|".'; return
                $holder = $ '#stage-holder'
                $stage = $ '<stage>'
                    .attr 'title',   title
                    .attr 'begin',   begin
                    .attr 'end',     end
                    .attr 'stageid', id
                    .appendTo $holder
                vStage = new Vue el: $stage[0]
                $title.val ''
                $begin.text ''
                $end.text ''
                $id.val ''
                $add.prop 'disabled', true
                $title.prop 'disabled', true
                d3.select("#tab-reports svg .tmp").classed 'tmp', false
    $tab = tabLoader id
    $stars = $tab.find('.cst-inactive')
    $holder = $ "#tab-#{id} .mdl-layout__content"
    $('#tab-reports .mdl-layout__header-row .mdl-menu').each ->
        cnt = $(@).find('li').length
        if cnt then new MaterialMenu @; return
        $(@).closest('.cst-popup').find('button').click ->
            alert 'Заполните справочник.'
    starClick = ->
        $stars.addClass 'cst-inactive'
        $(@).removeClass 'cst-inactive'
        $currentNavItem = $(@).closest('li').find('input')
        repId = $currentNavItem.attr 'name'
        arr = /(\d\d\d\d\-\d\d\-\d\dT\d\d\:\d\d)\-(\d+)\-(\d+)-(\d+)/.exec repId
        $el = $ "<report-form date=#{arr[1]} field=#{parseInt arr[2]} bush=#{parseInt arr[3]} well=#{parseInt arr[4]} reportid=#{repId} />"
        $holder.empty().append $el
        new Vue
            el: $el[0]
        obj = barn.get "#{repId}:form"
        if obj
            obj  = JSON.parse obj
            form = document.forms['report-form']
            form[key].value = value for key, value of obj when form[key] and value
        el = $tab.find('#svg-holder')[0]
        d3.select('#tab-reports .mdl-layout__header-row').style('visibility', 'visible')
        after = (svg, data, x)->

            formatTime   = d3.timeFormat "%Y-%m-%d %H:%M:%S"

            isAdding      = true
            zoneStart     = null
            $title        = $ '#stage-title'
            $beginLabel   = $ '#stage-begin'
            $endLabel     = $ '#stage-end'
            $stageId      = $ '#stage-id'
            $saveStage    = $ '#stage-adder'
            height        = svg.node().getBBox().height
            width         = svg.node().getBBox().width

            isSaved = ->
                not $stageId.val()

            sight = svg.append("line")
                .attr("class", "svg-sight")
                .attr("x1", 0)
                .attr("y1", 0)
                .attr("x2", 0)
                .attr("y2", height)

            svg.append("rect")
                .attr("class", "svg-overlay")
                .attr("width", width)
                .attr("height", height)
                .on "mousemove", ()->
                    _x = d3.mouse(this)[0]
                    sight.attr("transform", "translate( " + _x + ", 0 )")
                    dx = x.invert(_x)
                    if not isSaved() then return
                    if isAdding then $beginLabel.text formatTime dx
                    else $endLabel.text formatTime dx
                .on "click", ()->
                    _x = d3.mouse(this)[0]
                    if isAdding
                        zoneStart = svg.append("line")
                            .attr("class", "svg-start-selecting")
                            .attr("x1", _x)
                            .attr("y1", 0)
                            .attr("x2", _x)
                            .attr("y2", height)
                    else
                        tmp = svg.select('.tmp')
                        if tmp._groups[0][0]
                            if confirm 'Несохраненная зона будет удалена. Продолжить?' then tmp.remove()
                            else
                                zoneStart.remove()
                                isAdding = true
                                return
                        id = shortid.gen()
                        $stageId.val id
                        prevX = +zoneStart.attr('x1')
                        if prevX > _x then t = prevX; prevX = _x; _x = t
                        svg.append("rect")
                            .attr("class", "svg-selected-zone tmp")
                            .attr("width",  _x - prevX)
                            .attr("height", height)
                            .attr("id", id)
                            .attr("transform", "translate( " + prevX + ", 0 )")
                        svg.select('.svg-start-selecting').remove()
                        $title.text ''

                    isAdding = not isAdding
                    $saveStage.prop 'disabled', not isAdding
                    $title.prop 'disabled', not isAdding
            return unless obj and obj.stages
            $holder = $ '#tab-reports #stage-holder'
            obj.stages.split(';').forEach (el)->
                id = shortid.gen()
                arr = el.split '|'
                return unless arr.length is 3
                $stage = $ '<stage>'
                    .attr 'title',   arr[0]
                    .attr 'begin',   arr[1]
                    .attr 'end',     arr[2]
                    .attr 'stageid', id
                    .appendTo $holder
                date1 = parseTimeYMD arr[1]
                date2 = parseTimeYMD arr[2]
                if date2 < date1 then t = date2; date2 = date1; date1 = t
                setTimeout ->
                        _x1 = Math.round x(date1)
                        _x2 = Math.round x(date2)
                        svg.append("rect")
                            .attr("class", "svg-selected-zone")
                            .attr("width",  (_x2 - _x1))
                            .attr("height", height)
                            .attr("id", id)
                            .attr("transform", "translate( " + _x1 + ", 0 )")
                    , 1000
            new Vue
                el: $holder[0]
            $holder = $ '#tab-reports #stage-holder'
            $holder.data 'chart-data', data
        chart el, repId, null, null, after
    $stars.click starClick
    starClick
