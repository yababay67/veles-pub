define ['tabloader', 'vue', 'module', 'barn', 'reportid', 'reports'], (tabLoader, Vue, module, barn, getReportId, starClick)->
    id = module.id
    Vue.component "tab-body-#{id}",
        template: "#template-tab-#{id}"
    $tab = tabLoader id
    checkIsoDate = (value)->
        /^\d\d\d\d-(0?[1-9]|1[0-2])-(0?[1-9]|[12][0-9]|3[01]) (00|[0-9]|1[0-9]|2[0-3]):([0-9]|[0-5][0-9])$/.test value
    checkCsvFile = (value)->
        /\.csv$/.test value
    validator = new FormValidator 'upload-form', [
            {name: 'date',       rules: 'required|callback_iso_date'},
            {name: 'csv-file',   rules: 'required|callback_csv_file'},
            {name: 'field',      rules: 'numeric'},
            {name: 'well',       rules: 'numeric'},
            {name: 'bush',       rules: 'numeric'},
        ], (errors, event)->
                return unless errors.length > 0
                alert 'Заполните форму правильно.'
    $field = $tab.find '[name=field]'
    $well  = $tab.find '[name=well]'
    $bush  = $tab.find '[name=bush]'
    $date  = $tab.find '[name=date]'
    $tab.find('[name=csv-file]').change ->
        fln = $(@).val()
        reg = /(\d\d\d\d-\d\d-\d\dT\d\d.\d\d)-(\d+)-(\d+)-(\d+)/
        return unless reg.test fln
        arr = reg.exec fln
        $date.val arr[1].replace 'T', ' '
        $field.val +arr[2]
        $bush.val  +arr[3]
        $well.val  +arr[4]
    validator.registerCallback 'iso_date', checkIsoDate
    validator.registerCallback 'csv_file', checkCsvFile
    $iframe = $('iframe[name^=upload]')
    iframe  = $iframe[0]
    $iframe[0].onload = ->
        return unless not /json/.test iframe.location
        form  = document.forms['upload-form']
        date  = form.date.value
        field = form.field.value
        bush  = form.bush.value
        well  = form.well.value
        reportId = getReportId date, field, bush, well
        reportName = reportId.replace('T', ' ').replace '-', ' '
        barn.add 'reports', reportId, reportName
        $el = $ "<dict-item itemid=#{reportId}, itemvalue=\'#{reportName}\' deletable=true clickable=true />"
        $ul = $("#tab-reports nav ul").prepend $el
        new Vue
            el: $el[0]
            mounted: ->
                $(@$el).find('i:nth-child(1)').click starClick
        form.reset()
        alert 'Данные выгружены'
        $iframe[0].location = '/upload-stub.html'
    null
