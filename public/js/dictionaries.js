(function() {
  define(['tabloader', 'vue', 'module', 'barn'], function(tabLoader, Vue, module, barn) {
    var $tab, buildDict, id, markup, ref;
    id = module.id;
    Vue.component("tab-body-" + id, {
      template: "#template-tab-" + id
    });
    $tab = tabLoader(id);
    buildDict = function(id, markup) {
      var $card, $markup;
      $card = $tab.find("#" + id + "-dict .mdl-card__supporting-text");
      if (!$card.length) {
        return;
      }
      $markup = $(markup).attr('dictid', id).attr('dictclass', 'cst-dict-list');
      $card = $card.append($markup).parent();
      if ($markup.attr('addable') === 'true') {
        $("<dict-adder dictid=" + id + ">").attr('editable', 'true' === $markup.attr('editable')).attr('deletable', 'true' === $markup.attr('deletable')).attr('checkable', 'true' === $markup.attr('checkable')).appendTo($card);
      }
      return new Vue({
        el: $card[0]
      });
    };
    ref = {
      fields: '<dict deletable="true" addable="true"  editable="true">',
      devices: '<dict deletable="true" addable="false" editable="true">',
      staff: '<dict deletable="true" addable="true"  editable="true">',
      customers: '<dict deletable="true" addable="true"  editable="true">',
      supervisors: '<dict deletable="true" addable="true"  editable="true">',
      welltypes: '<dict deletable="true" addable="true"  editable="true">',
      worktypes: '<dict deletable="true" addable="true"  editable="true">'
    };
    for (id in ref) {
      markup = ref[id];
      buildDict(id, markup);
    }
    return null;
  });

}).call(this);
