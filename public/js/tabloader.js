(function() {
  define(['jquery', 'vue'], function($, Vue) {
    return function(id) {
      var $holder, $tab;
      $tab = $("#tab-" + id);
      $holder = $tab.find('.cst-hidden');
      $holder.append("<tab-body-" + id + ">");
      new Vue({
        el: $holder[0]
      });
      $holder = $tab.find('.cst-hidden');
      setTimeout(function() {
        $tab.find('.cst-progress-wrapper').remove();
        $holder.removeClass('cst-hidden');
        return null;
      }, 1000);
      return $tab;
    };
  });

}).call(this);
