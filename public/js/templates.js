(function() {
  define(['vue', 'barn', 'validators'], function(Vue, barn, validators) {
    Vue.component('chart-footer-radio', {
      template: '#template-chart-footer-radio',
      props: ['paramid'],
      computed: {
        title: function() {
          if (this.paramid === 'all') {
            return 'всё';
          }
          return (barn.get(this.paramid) || this.paramid).toLowerCase();
        }
      },
      methods: {
        showLines: function() {
          return alert(123);
        }
      }
    });
    Vue.component('chart-footer', {
      template: '#template-chart-footer',
      props: ['chartid']
    });
    Vue.component('mdl-dialog', {
      template: '#template-mdl-dialog',
      props: ['title', 'dlgid']
    });
    Vue.component('card-10-of-12-holder', {
      template: '#template-card-10-of-12-holder',
      props: ['title', 'cardid']
    });
    Vue.component('card-8-of-12-holder', {
      template: '#template-card-8-of-12-holder',
      props: ['title', 'cardid']
    });
    Vue.component('card-6-of-12-holder', {
      template: '#template-card-6-of-12-holder',
      props: ['title', 'cardid']
    });
    Vue.component('card-4-of-12-holder', {
      template: '#template-card-4-of-12-holder',
      props: ['title', 'cardid']
    });
    Vue.component('card-3-of-12-holder', {
      template: '#template-card-3-of-12-holder',
      props: ['title', 'cardid']
    });
    Vue.component('card-6-of-12', {
      template: '#template-card-6-of-12',
      props: ['title']
    });
    Vue.component('card-8-of-12', {
      template: '#template-card-8-of-12',
      props: ['title']
    });
    Vue.component('card-10-of-12', {
      template: '#template-card-10-of-12',
      props: ['title']
    });
    Vue.component('card-4-of-12', {
      template: '#template-card-4-of-12',
      props: ['title']
    });
    Vue.component('card-3-of-12', {
      template: '#template-card-3-of-12',
      props: ['title']
    });
    Vue.component('row-1-of-3', {
      template: '#template-row-1-of-3',
      props: ['title', 'cardid']
    });
    Vue.component('row-1-of-1-6', {
      template: '#template-row-1-of-1-6',
      props: ['title', 'cardid']
    });
    Vue.component('row-1-of-1-8', {
      template: '#template-row-1-of-1-8',
      props: ['title', 'cardid']
    });
    Vue.component('row-1-of-1-10', {
      template: '#template-row-1-of-1-10',
      props: ['title', 'cardid']
    });
    Vue.component('row-2-of-4', {
      template: '#template-row-2-of-4',
      props: ['title1', 'title2', 'cardid1', 'cardid2']
    });
    Vue.component('row-3-of-3', {
      template: '#template-row-3-of-3',
      props: ['title1', 'title2', 'title3', 'cardid1', 'cardid2', 'cardid3']
    });
    Vue.component('row-4-of-4', {
      template: '#template-row-4-of-4-cards',
      props: ['title1', 'title2', 'title3', 'title4', 'cardid1', 'cardid2', 'cardid3', 'cardid4']
    });
    Vue.component('popup-menu', {
      template: '#template-popup-menu',
      props: ['dict', 'label'],
      methods: {
        getItems: function() {
          var arr;
          arr = barn.smembers(this.dict.replace('-choice', ''));
          if (!arr) {
            return [];
          }
          return arr.map(function(el) {
            return {
              id: el,
              name: barn.get(el)
            };
          });
        }
      }
    });
    Vue.component('popup-item', {
      template: '#template-popup-item',
      props: ['item', 'name', 'dict'],
      methods: {
        selectIt: function() {
          var $form, dict, id, isFound, nm;
          isFound = false;
          dict = this.dict;
          id = this.itemid;
          nm = this.name;
          $form = $(this.$el).closest('.mdl-layout').find('#report-fields');
          ['', '-1', '-2', '-3', '-4', '-5'].forEach(function(el) {
            var arr, txt;
            if (!!isFound) {
              return;
            }
            arr = $form.find("[name=of-" + dict + el + "]");
            if (!arr.length) {
              return;
            }
            txt = $(arr[0]).val().trim();
            if (!(!txt || !el)) {
              return;
            }
            $(arr[0]).val(nm);
            return isFound = true;
          });
          return null;
        }
      }
    });
    Vue.component('dict', {
      template: '#template-dictionary',
      props: ['dictid', 'items', 'deletable', 'addable', 'dictclass', 'reverse', 'clickable', 'editable', 'defaults'],
      methods: {
        getValue: function(id) {
          return barn.get(id) || this.getDefault(id) || id;
        },
        getItems: function() {
          var arr;
          if (!!this.items) {
            return this.items.split(' ');
          }
          arr = barn.smembers(this.dictid) || [];
          if (this.reverse !== 'true') {
            return arr;
          }
          return arr.reverse();
        },
        getDefault: function(id) {
          if (typeof this.defaults !== 'object') {
            return null;
          }
          return this.defaults[id];
        }
      },
      mounted: function() {
        var dfl, keys, vals;
        if (!(this.items && this.defaults)) {
          return;
        }
        keys = this.items.split(' ');
        vals = this.defaults.split(/;\ */);
        this.defaults = {};
        dfl = this.defaults;
        return keys.forEach(function(el, i) {
          var val;
          val = vals[i];
          dfl[el] = val;
          if (!!val) {
            return barn.set(el, val);
          }
        });
      }
    });
    Vue.component('dict-item', {
      template: '#template-dictionary-item',
      props: ['itemid', 'itemvalue', 'deletable', 'editable', 'clickable'],
      mounted: function() {
        return $(this.$el).on("focusout", "input", function() {
          var key;
          if (!!$(this).prop('disabled')) {
            return;
          }
          key = $(this).prop('disabled', true).removeClass('cst-input-enabled').attr('name');
          return barn.set(key, $(this).val());
        });
      },
      methods: {
        clickIt: function() {
          return null;
        },
        editIt: function() {
          return $(this.$el).find("input").prop('disabled', false).addClass('cst-input-enabled').focus();
        },
        saveIt: function() {
          var isValid, val;
          val = $(this.$el).find("input").prop('disabled', true).removeClass('cst-input-enabled').val();
          isValid = validators[this.$parent.dictid];
          if (isValid && isValid(val)) {
            barn.set(this.itemid, val);
            return;
          }
          return barn.set(this.itemid, val);
        },
        deleteIt: function() {
          if (!confirm('Действительно удалить?')) {
            return;
          }
          barn.rem(this.$parent.dictid, this.itemid);
          return $(this.$el).remove();
        }
      }
    });
    Vue.component('dict-adder', {
      template: '#template-dictionary-adder',
      props: ['dictid', 'editable', 'deletable', 'checkable'],
      methods: {
        addIt: function() {
          var $el, $inp, id, isValid, val;
          isValid = validators[this.dictid];
          $inp = $(this.$el).find('input');
          val = $inp.val().trim();
          if (!((typeof isValid === 'function' && isValid(val)) || !isValid)) {
            return;
          }
          $inp.val('');
          id = barn.add(this.dictid, val);
          $el = $("<dict-item itemid=" + id + ">").attr('itemvalue', val).attr('editable', this.editable === 'true').attr('deletable', this.deletable === 'true').attr('chekable', this.checkable === 'true').appendTo("#" + this.dictid + " ul");
          return new Vue({
            el: $el[0]
          });
        }
      }
    });
    Vue.component('form-field', {
      template: '#template-form-field',
      props: ['input', 'label', 'wide', 'type', 'inputvalue', 'disabled'],
      methods: {
        setFileName: function() {
          return $('#uploadable-file-name').val($('#uploadable-file').val());
        }
      }
    });
    return null;
  });

}).call(this);
