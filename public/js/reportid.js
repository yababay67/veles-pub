(function() {
  var BUSH_ZEROS, FIELD_ZEROS, WELL_ZEROS, getReportId, leadingZeros;

  FIELD_ZEROS = 2;

  BUSH_ZEROS = 6;

  WELL_ZEROS = 6;

  leadingZeros = function(num, count) {
    while (num.length < count) {
      num = "0" + num;
    }
    return num;
  };

  getReportId = function(date, field, bush, well) {
    return (date.replace(' ', 'T').replace(':', '-')) + "-" + (leadingZeros(field, FIELD_ZEROS)) + "-" + (leadingZeros(bush, BUSH_ZEROS)) + "-" + (leadingZeros(well, WELL_ZEROS));
  };

  if (typeof window !== 'undefined') {
    define(function() {
      return getReportId;
    });
  } else {
    module.exports = getReportId;
  }

}).call(this);
