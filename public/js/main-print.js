(function() {
  require.config({
    waitSeconds: 30,
    baseUrl: '/js',
    paths: {
      d3v4: 'vendor/d3.v4.min',
      barnlib: 'vendor/barn.min',
      shortid: 'vendor/js-shortid.min',
      vue: 'vendor/vue.min',
      jquery: 'vendor/jquery.min',
      moment: 'vendor/moment-with-locales.min'
    }
  });

  window.onload = function() {
    return require(['print']);
  };

}).call(this);
