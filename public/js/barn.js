(function() {
  define(['barnlib', 'shortid'], function(Barn, shortid) {
    var barn;
    Barn.prototype.add = function(dict, id, val) {
      if (!val) {
        val = id;
        id = shortid.gen();
      }
      this.set(id, val);
      this.sadd(dict, id);
      return id;
    };
    Barn.prototype.rem = function(dict, id) {
      this.del(id);
      return this.srem(dict, id);
    };
    barn = new Barn(localStorage);
    return barn;
  });

}).call(this);
