(function() {
  define(['d3v4', 'shortid', 'vue', 'barn'], function(d3, shortid, Vue, barn) {
    return function(el, url, width, height, after) {
      var bisectDate, consumptionLine, densityLine, drawChart, formatTime, margin, parseTimeDMY, parseTimeYMD, pressure1Line, pressure2Line, svg, temperatureLine, volumeLine, x, y0, y1, y2, y3, y4, y5;
      if (width == null) {
        width = 960;
      }
      if (height == null) {
        height = 500;
      }
      $(el).empty();
      margin = {
        top: 20,
        right: 60,
        bottom: 30,
        left: 50
      };
      width = width - margin.left - margin.right;
      height = height - margin.top - margin.bottom;
      parseTimeDMY = d3.timeParse("%d.%m.%Y %H:%M:%S");
      parseTimeYMD = d3.timeParse("%Y-%m-%d %H:%M:%S");
      formatTime = d3.timeFormat("%Y-%m-%d %H:%M:%S");
      bisectDate = d3.bisector(function(d) {
        return d.date;
      }).left;
      x = d3.scaleTime().range([0, width]);
      y0 = d3.scaleLinear().range([height, 0]);
      y1 = d3.scaleLinear().range([height, 0]);
      y2 = d3.scaleLinear().range([height, 0]);
      y3 = d3.scaleLinear().range([height, 0]);
      y4 = d3.scaleLinear().range([height, 0]);
      y5 = d3.scaleLinear().range([height, 0]);
      volumeLine = d3.line().x(function(d) {
        return x(d.date);
      }).y(function(d) {
        return y0(d.volume);
      });
      consumptionLine = d3.line().x(function(d) {
        return x(d.date);
      }).y(function(d) {
        return y1(d.consumption);
      });
      temperatureLine = d3.line().x(function(d) {
        return x(d.date);
      }).y(function(d) {
        return y2(d.temperature);
      });
      pressure1Line = d3.line().x(function(d) {
        return x(d.date);
      }).y(function(d) {
        return y3(d.pressure1);
      });
      pressure2Line = d3.line().x(function(d) {
        return x(d.date);
      }).y(function(d) {
        return y4(d.pressure2);
      });
      densityLine = d3.line().x(function(d) {
        return x(d.date);
      }).y(function(d) {
        return y5(d.density);
      });
      svg = d3.select(el).append("svg").attr("width", width + margin.left + margin.right).attr("height", height + margin.top + margin.bottom).append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");
      drawChart = function(data) {
        var chartFooter, normField, obj, stages;
        normField = function(k, f, d) {
          if (typeof f === 'string') {
            f = f.replace(',', '.');
          }
          if (isNaN(f)) {
            f = 0;
          }
          return d[k] = +f;
        };
        data.forEach(function(d) {
          var k, results, v;
          if (typeof d.date !== 'string') {
            return;
          }
          d.date = parseTimeDMY(d.date);
          results = [];
          for (k in d) {
            v = d[k];
            if (k !== 'date') {
              results.push(normField(k, v, d));
            }
          }
          return results;
        });
        x.domain(d3.extent(data, function(d) {
          return d.date;
        }));
        y0.domain([
          0, d3.max(data, function(d) {
            return Math.max(d.volume);
          })
        ]);
        y1.domain([
          0, d3.max(data, function(d) {
            return Math.max(d.consumption);
          })
        ]);
        svg.append("g").attr("transform", "translate(0," + height + ")").call(d3.axisBottom(x));
        svg.append("g").attr("class", "axisSteelBlue").call(d3.axisLeft(y0));
        svg.append("g").attr("class", "axisRed").attr("transform", "translate( " + width + ", 0 )").call(d3.axisRight(y1));
        svg.append("path").data([data]).attr("class", "svg-volume").attr("d", volumeLine);
        svg.append("path").data([data]).attr("class", "svg-consumption").attr("d", consumptionLine);
        svg.append("path").data([data]).attr("class", "svg-temperature").attr("d", temperatureLine);
        svg.append("path").data([data]).attr("class", "svg-pressure1").attr("d", pressure1Line);
        svg.append("path").data([data]).attr("class", "svg-pressure2").attr("d", pressure2Line);
        svg.append("path").data([data]).attr("class", "svg-density").attr("d", densityLine);
        if (typeof url !== 'string') {
          return;
        }
        obj = JSON.parse(barn.get(url + ":form"));
        if (obj && obj.stages && obj.stages.split(';').length) {
          stages = obj.stages;
          stages.split(';').forEach(function(el) {
            var arr, date1, date2, id, t;
            id = shortid.gen();
            arr = el.split('|');
            if (arr.length !== 3) {
              return;
            }
            date1 = parseTimeYMD(arr[1]);
            date2 = parseTimeYMD(arr[2]);
            if (date2 < date1) {
              t = date2;
              date2 = date1;
              date1 = t;
            }
            return setTimeout(function() {
              var _x1, _x2;
              _x1 = Math.round(x(date1));
              _x2 = Math.round(x(date2));
              svg.append("rect").attr("class", "svg-selected-zone").attr("width", _x2 - _x1).attr("height", height).attr("id", id).attr("transform", "translate( " + _x1 + ", 0 )");
              return svg.append("text").attr("class", "svg-zone-label").attr("x", _x2 - 15).attr("y", 10).attr("style", "writing-mode: tb; glyph-orientation-vertical: 0;").text(arr[0]);
            }, 1000);
          });
        }
        if (!/print\.html/.test(window.location.pathname)) {
          chartFooter = $("<chart-footer chartid=\'" + (shortid.gen()) + "\'>").insertAfter(svg.node().parentNode);
          new Vue({
            el: chartFooter[0]
          });
        }
        if (typeof after !== 'function') {
          return;
        }
        after(svg, data, x);
        return null;
      };
      if (typeof url !== 'string') {
        drawChart(url);
        return;
      }
      d3.csv("/data/" + url + ".csv?r=" + (Math.random()), function(err, dat) {
        if (err) {
          throw err;
        }
        return drawChart(dat);
      });
      return null;
    };
  });

}).call(this);
