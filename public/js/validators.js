(function() {
  define(function() {
    return {
      fields: function(val) {
        var arr;
        if (!isNaN(val)) {
          if (+val < 1000) {
            return true;
          } else {
            alert('Номер месторождения должен быть меньше 1000.');
            return false;
          }
        }
        arr = /.+\((\d+)\)$/.exec(val);
        if (arr && arr.length > 0 && !isNaN(arr[1])) {
          return true;
        }
        alert('Укажите номер в скобках. Пример: "Самотлор (123)".');
        return false;
      },
      bushes: function(val) {
        if (!val || isNaN(val)) {
          alert('Поле "Куст" должно содержать число');
          return false;
        }
        return true;
      },
      wells: function(val) {
        if (!val || isNaN(val)) {
          alert('Поле "Скважина" должно содержать число');
          return false;
        }
        return true;
      },
      isoDate: function(val) {
        if (!val || !/^\d\d\d\d\-\d\d\-\d\d\ \d\d\:\d\d$/.test(val)) {
          alert('Некорректно указана дата. Формат ГГГГ-ММ-ДД ЧЧ:ММ.');
          return false;
        }
        return true;
      }
    };
  });

}).call(this);
