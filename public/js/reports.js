(function() {
  define(['tabloader', 'vue', 'module', 'barn', 'reportchart', 'd3v4', 'reportid', 'validators', 'shortid', 'dialog', 'moment'], function(tabLoader, Vue, module, barn, chart, d3, getReportId, validators, shortid, dialog, moment) {
    var $chartDialog, $currentNavItem, $holder, $stars, $tab, chartDialog, id, parseTimeYMD, starClick;
    id = module.id;
    $currentNavItem = null;
    $chartDialog = $('.cst-chart-dialog');
    chartDialog = $chartDialog[0];
    dialog.registerDialog(chartDialog);
    $(chartDialog).find('button').click(function() {
      return chartDialog.close();
    });
    parseTimeYMD = d3.timeParse("%Y-%m-%d %H:%M:%S");
    Vue.component("stage", {
      template: "#template-report-form-stage",
      props: ['title', 'begin', 'end', 'stageid', 'chartData'],
      methods: {
        deleteRow: function() {
          if (!confirm('Действительно удалить?')) {
            return;
          }
          d3.select("#tab-reports svg [id=" + this.stageid).remove();
          return $(this.$el).remove();
        },
        viewRow: function() {
          var $holder, $svgHolder, d1, d2, t;
          if (!this.chartData) {
            d1 = moment(this.begin);
            d2 = moment(this.end);
            if (moment(d2).isBefore(d1)) {
              t = d2;
              d2 = d1;
              d1 = t;
            }
            $holder = $('#tab-reports #stage-holder');
            this.chartData = $holder.data('chart-data').filter(function(el) {
              var mom;
              mom = moment(el.date);
              return mom.isAfter(d1) && mom.isBefore(d2);
            });
          }
          $svgHolder = $chartDialog.find('.chart-holder');
          require('reportchart')($svgHolder[0], this.chartData);
          $chartDialog.find('h6').text(this.title + " (" + (this.begin.replace(/\d\d\d\d-\d\d-\d\d /, '')) + " - " + (this.end.replace(/\d\d\d\d-\d\d-\d\d /, '')) + ")");
          return chartDialog.showModal();
        }
      },
      mounted: function() {
        var $holder;
        $holder = $('#tab-reports #stage-holder');
        return this.chartData = $holder.data('chart-data');
      }
    });
    Vue.component("tab-body-" + id, {
      template: "#template-tab-" + id
    });
    Vue.component("report-form", {
      template: "#template-report-form",
      props: ['date', 'field', 'bush', 'well', 'reportid', 'title'],
      mounted: function() {
        var $title;
        return $title = $('[name=stage-title]').prop('disabled', true).attr('id', 'stage-title');
      },
      methods: {
        printReport: function() {
          this.saveReport(true);
          return window.open("/print.html#" + this.reportid, '_blank');
        },
        saveReport: function(mute) {
          var arr, fld, form, inp, key, obj, reportName, stages;
          form = document.forms['report-form'];
          if (!validators['fields'](form['of-fields'].value)) {
            return;
          }
          if (!validators['bushes'](form['bush'].value)) {
            return;
          }
          if (!validators['wells'](form['well'].value)) {
            return;
          }
          if (!validators['isoDate'](form['date'].value)) {
            return;
          }
          obj = {};
          stages = '';
          $('#tab-reports #stage-holder tr').each(function() {
            var n;
            n = 0;
            return $(this).find('td').each(function() {
              var delim;
              if (!(++n < 4)) {
                return;
              }
              delim = '|';
              if (n === 3) {
                delim = ';';
              }
              return stages += "" + ($(this).text()) + delim;
            });
          });
          for (key in form) {
            inp = form[key];
            if (inp && inp.value) {
              obj[$(inp).attr('name')] = inp.value;
            }
          }
          if (stages) {
            obj.stages = stages;
          }
          fld = obj['of-fields'];
          if (isNaN(fld)) {
            arr = /.+\((\d+)\)$/.exec(fld);
            fld = arr[1];
          }
          reportName = getReportId(obj.date, fld, obj.bush, obj.well).replace(obj.date + "-", obj.date + " ").replace('T', ' ');
          barn.set(this.reportid, reportName);
          $currentNavItem.val(reportName);
          barn.set(this.reportid + ":form", JSON.stringify(obj));
          if (!mute) {
            return alert('Данные сохранены.');
          }
        },
        getName: function() {
          return barn.get(this.reportid);
        },
        addStage: function() {
          var $add, $begin, $end, $holder, $id, $stage, $title, begin, end, t, title, vStage;
          $title = $('#stage-title');
          $begin = $('#stage-begin');
          $end = $('#stage-end');
          $id = $('#stage-id');
          $add = $('#stage-adder');
          title = $title.val().trim();
          begin = $begin.text();
          end = $end.text();
          if (parseTimeYMD(begin) > parseTimeYMD(end)) {
            t = begin;
            begin = end;
            end = t;
          }
          id = $id.val();
          if (!title) {
            alert('Укажите название этапа.');
            return;
          }
          if (/[\;\|]/.test(title)) {
            alert('Название этапа не должно содержать символов ";" и "|".');
            return;
          }
          $holder = $('#stage-holder');
          $stage = $('<stage>').attr('title', title).attr('begin', begin).attr('end', end).attr('stageid', id).appendTo($holder);
          vStage = new Vue({
            el: $stage[0]
          });
          $title.val('');
          $begin.text('');
          $end.text('');
          $id.val('');
          $add.prop('disabled', true);
          $title.prop('disabled', true);
          return d3.select("#tab-reports svg .tmp").classed('tmp', false);
        }
      }
    });
    $tab = tabLoader(id);
    $stars = $tab.find('.cst-inactive');
    $holder = $("#tab-" + id + " .mdl-layout__content");
    $('#tab-reports .mdl-layout__header-row .mdl-menu').each(function() {
      var cnt;
      cnt = $(this).find('li').length;
      if (cnt) {
        new MaterialMenu(this);
        return;
      }
      return $(this).closest('.cst-popup').find('button').click(function() {
        return alert('Заполните справочник.');
      });
    });
    starClick = function() {
      var $el, after, arr, el, form, key, obj, repId, value;
      $stars.addClass('cst-inactive');
      $(this).removeClass('cst-inactive');
      $currentNavItem = $(this).closest('li').find('input');
      repId = $currentNavItem.attr('name');
      arr = /(\d\d\d\d\-\d\d\-\d\dT\d\d\:\d\d)\-(\d+)\-(\d+)-(\d+)/.exec(repId);
      $el = $("<report-form date=" + arr[1] + " field=" + (parseInt(arr[2])) + " bush=" + (parseInt(arr[3])) + " well=" + (parseInt(arr[4])) + " reportid=" + repId + " />");
      $holder.empty().append($el);
      new Vue({
        el: $el[0]
      });
      obj = barn.get(repId + ":form");
      if (obj) {
        obj = JSON.parse(obj);
        form = document.forms['report-form'];
        for (key in obj) {
          value = obj[key];
          if (form[key] && value) {
            form[key].value = value;
          }
        }
      }
      el = $tab.find('#svg-holder')[0];
      d3.select('#tab-reports .mdl-layout__header-row').style('visibility', 'visible');
      after = function(svg, data, x) {
        var $beginLabel, $endLabel, $saveStage, $stageId, $title, formatTime, height, isAdding, isSaved, sight, width, zoneStart;
        formatTime = d3.timeFormat("%Y-%m-%d %H:%M:%S");
        isAdding = true;
        zoneStart = null;
        $title = $('#stage-title');
        $beginLabel = $('#stage-begin');
        $endLabel = $('#stage-end');
        $stageId = $('#stage-id');
        $saveStage = $('#stage-adder');
        height = svg.node().getBBox().height;
        width = svg.node().getBBox().width;
        isSaved = function() {
          return !$stageId.val();
        };
        sight = svg.append("line").attr("class", "svg-sight").attr("x1", 0).attr("y1", 0).attr("x2", 0).attr("y2", height);
        svg.append("rect").attr("class", "svg-overlay").attr("width", width).attr("height", height).on("mousemove", function() {
          var _x, dx;
          _x = d3.mouse(this)[0];
          sight.attr("transform", "translate( " + _x + ", 0 )");
          dx = x.invert(_x);
          if (!isSaved()) {
            return;
          }
          if (isAdding) {
            return $beginLabel.text(formatTime(dx));
          } else {
            return $endLabel.text(formatTime(dx));
          }
        }).on("click", function() {
          var _x, prevX, t, tmp;
          _x = d3.mouse(this)[0];
          if (isAdding) {
            zoneStart = svg.append("line").attr("class", "svg-start-selecting").attr("x1", _x).attr("y1", 0).attr("x2", _x).attr("y2", height);
          } else {
            tmp = svg.select('.tmp');
            if (tmp._groups[0][0]) {
              if (confirm('Несохраненная зона будет удалена. Продолжить?')) {
                tmp.remove();
              } else {
                zoneStart.remove();
                isAdding = true;
                return;
              }
            }
            id = shortid.gen();
            $stageId.val(id);
            prevX = +zoneStart.attr('x1');
            if (prevX > _x) {
              t = prevX;
              prevX = _x;
              _x = t;
            }
            svg.append("rect").attr("class", "svg-selected-zone tmp").attr("width", _x - prevX).attr("height", height).attr("id", id).attr("transform", "translate( " + prevX + ", 0 )");
            svg.select('.svg-start-selecting').remove();
            $title.text('');
          }
          isAdding = !isAdding;
          $saveStage.prop('disabled', !isAdding);
          return $title.prop('disabled', !isAdding);
        });
        if (!(obj && obj.stages)) {
          return;
        }
        $holder = $('#tab-reports #stage-holder');
        obj.stages.split(';').forEach(function(el) {
          var $stage, date1, date2, t;
          id = shortid.gen();
          arr = el.split('|');
          if (arr.length !== 3) {
            return;
          }
          $stage = $('<stage>').attr('title', arr[0]).attr('begin', arr[1]).attr('end', arr[2]).attr('stageid', id).appendTo($holder);
          date1 = parseTimeYMD(arr[1]);
          date2 = parseTimeYMD(arr[2]);
          if (date2 < date1) {
            t = date2;
            date2 = date1;
            date1 = t;
          }
          return setTimeout(function() {
            var _x1, _x2;
            _x1 = Math.round(x(date1));
            _x2 = Math.round(x(date2));
            return svg.append("rect").attr("class", "svg-selected-zone").attr("width", _x2 - _x1).attr("height", height).attr("id", id).attr("transform", "translate( " + _x1 + ", 0 )");
          }, 1000);
        });
        new Vue({
          el: $holder[0]
        });
        $holder = $('#tab-reports #stage-holder');
        return $holder.data('chart-data', data);
      };
      return chart(el, repId, null, null, after);
    };
    $stars.click(starClick);
    return starClick;
  });

}).call(this);
