(function() {
  define(['barn', 'moment', 'vue', 'reportchart', 'jquery'], function(barn, moment, Vue) {
    var $header, after, cnt, date, docnum, el, hash, obj;
    hash = window.location.hash;
    if (!hash || !/^\#\d\d\d\d\-/.test(hash)) {
      alert('Не указан идентификатор отчета');
      return;
    }
    docnum = prompt('Введите номер документа');
    if (!docnum) {
      docnum = ' _____ ';
    }
    date = ' __________________ ';
    moment.locale('ru');
    if (confirm('Использовать сегодняшнюю дату?')) {
      date = moment().format('LL');
    }
    Vue.component("top-grid", {
      template: "#top-grid",
      props: ['customer', 'begin', 'duration', 'date', 'docnum', 'staff1', 'staff2', 'staff3', 'supervisor1', 'supervisor2', 'supervisor3', 'field', 'bush', 'well', 'volume', 'density', 'welltype', 'worktype'],
      computed: {
        performer: function() {
          var param, perf;
          perf = '';
          param = barn.get("performers-name");
          if (param) {
            perf += param;
          }
          param = barn.get("performers-inn");
          if (param) {
            perf += " ИНН: " + param + ",";
          }
          param = barn.get("performers-address");
          if (param) {
            perf += " адрес: " + param + ",";
          }
          param = barn.get("performers-phone");
          if (param) {
            perf += " тел.: " + param + ",";
          }
          param = barn.get("performers-email");
          if (param) {
            perf += " e-mail.: " + param + ",";
          }
          return perf.replace(/\,$/, '');
        }
      }
    });
    hash = hash.substring(1);
    cnt = 0;
    obj = JSON.parse(barn.get(hash + ":form"));
    $header = $('header');
    after = function(svg, data, x) {
      var $topGrid, density, dur, durH, durM, fieldsAvg, stages, volume;
      fieldsAvg = function(_arr, fld) {
        return Math.round((_arr.map(function(el) {
          return el[fld];
        })).reduce(function(a, b) {
          return a + b;
        }) / _arr.length * 100) / 100;
      };
      volume = 0;
      data.forEach(function(el) {
        if (el.volume > volume) {
          return volume = el.volume;
        }
      });
      density = fieldsAvg(data, 'density');
      dur = moment.duration(moment(data[0].date).diff(data[data.length - 1].date));
      durH = Math.abs(dur.hours());
      durM = Math.abs(dur.minutes()) % 60;
      $topGrid = $("<top-grid>").attr('docnum', docnum).attr('date', date).attr('customer', obj.customer).attr('staff1', obj['of-staff-1']).attr('staff2', obj['of-staff-2']).attr('staff3', obj['of-staff-3']).attr('supervisor1', obj['of-supervisors-1']).attr('supervisor2', obj['of-supervisors-2']).attr('supervisor3', obj['of-supervisors-3']).attr('field', obj['of-fields'].replace(/\(\d+\)/, '')).attr('bush', obj['bush']).attr('well', obj['well']).attr('welltype', obj['welltype']).attr('worktype', obj['worktype']).attr('volume', volume).attr('density', density).attr('begin', moment(data[0].date).format('lll')).attr('duration', durH + " ч. " + durM + " мин.").appendTo($header);
      new Vue({
        el: $topGrid[0]
      });
      if (!obj.stages) {
        return;
      }
      stages = obj.stages.split(';');
      if (!(stages && stages.length)) {
        return;
      }
      return stages.forEach(function(el) {
        var $table, aft, arr, bfr, consAvg, d1, d2, densAvg, diff, farr, pressAvg, t, volume1, volume2;
        arr = el.split('|');
        if (arr.length !== 3) {
          return;
        }
        d1 = moment(arr[1]);
        d2 = moment(arr[2]);
        if (moment(d2).isBefore(d1)) {
          t = d2;
          d2 = d1;
          d1 = t;
        }
        diff = d2.diff(d1);
        durM = Math.round(moment.duration(diff).asMinutes());
        durH = Math.round(moment.duration(diff).asHours());
        if (durH) {
          dur = durH + " ч. ";
        } else {
          durH = '';
        }
        dur = "" + durH + durM + " мин.";
        bfr = true;
        aft = false;
        farr = data.filter(function(el) {
          var mom;
          mom = moment(el.date);
          return mom.isAfter(d1) && mom.isBefore(d2);
        });
        volume1 = farr[0].volume;
        volume2 = farr[farr.length - 1].volume;
        consAvg = fieldsAvg(farr, 'consumption');
        densAvg = fieldsAvg(farr, 'density');
        pressAvg = fieldsAvg(farr, 'pressure1');
        $table = $('#stages tbody');
        return $("<tr>\n    <td align='center'>" + (++cnt) + ".</td>\n    <td>" + arr[0] + "</td>\n    <td align='right'>" + dur + "</td>\n    <td align='right'>" + (volume2 - volume1) + "</td>\n    <td align='right'>" + consAvg + "</td>\n    <td align='right'>" + densAvg + "</td>\n    <td align='right'>" + pressAvg + "</td>\n</tr>").appendTo($table);
      });
    };
    el = $('#svg-holder')[0];
    require('reportchart')(el, hash, 1100, 700, after);
    return null;
  });

}).call(this);
