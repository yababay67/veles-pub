(function() {
  define(['tabloader', 'vue', 'module', 'barn'], function(tabLoader, Vue, module, barn) {
    var $tab, buildDict, fields, form, id, markup, ref;
    fields = ['name', 'phone', 'email', 'address', 'inn'];
    id = module.id;
    Vue.component("tab-body-" + id, {
      template: "#template-tab-" + id,
      methods: {
        savePerformersData: function() {
          return null;
        }
      },
      mounted: function() {
        return fields.forEach(function(el) {
          var key, val;
          key = "performers-" + el;
          val = barn.get(key);
          if (!val) {
            return;
          }
          return $(this.$el).find("[name=" + key + "]").val(val);
        });
      }
    });
    $tab = tabLoader(id);
    form = document.forms['performers-form'];
    form.onsubmit = function(e) {
      var logo;
      fields.forEach(function(el) {
        var inp, key;
        key = "performers-" + el;
        inp = form[key];
        if (!(inp && inp.value)) {
          return;
        }
        return barn.set(key, inp.value);
      });
      logo = form['performers-logo'].value;
      alert('no logo');
      e.preventDefault();
      if (!(logo && /(\.png|\.jpg|\.jpeg)$/.test(logo))) {
        return false;
      }
      return true;
    };
    buildDict = function(id, markup) {
      var $card, $markup;
      $card = $tab.find("#" + id + "-dict .mdl-card__supporting-text");
      if (!$card.length) {
        return;
      }
      $markup = $(markup).attr('dictid', id).attr('dictclass', 'cst-parameters-list');
      $card = $card.append($markup).parent();
      if ($markup.attr('addable') === 'true') {
        $("<dict-adder dictid=" + id + ">").attr('editable', 'true' === $markup.attr('editable')).attr('deletable', 'true' === $markup.attr('deletable')).attr('checkable', 'true' === $markup.attr('checkable')).appendTo($card);
      }
      return new Vue({
        el: $card[0]
      });
    };
    ref = {
      parameters: '<dict\n    deletable="false" addable="false"  editable="true"\n    items="volume consumption pressure1 pressure2 pressure3 temperature density"\n    defaults="Объем; Расход; Давление 1; Давление 2; Давление 3; Температура; Плотность"\n>'
    };
    for (id in ref) {
      markup = ref[id];
      buildDict(id, markup);
    }
    return null;
  });

}).call(this);
