(function() {
  var Controller, getReportId, locomotive, multiparty, publicDir, shell, uploadController;

  publicDir = '../../../public';

  getReportId = require(publicDir + "/js/reportid.js");

  locomotive = require('locomotive');

  multiparty = require('multiparty');

  shell = require('shelljs');

  Controller = locomotive.Controller;

  uploadController = new Controller;

  uploadController.logo = function() {
    var ctrl, form, res;
    res = this.res;
    ctrl = this;
    form = new multiparty.Form();
    return form.parse(this.req, function(err, fields, files) {
      var ext, hasFile, isJpeg, newPath, tmpPath;
      tmpPath = files['performers-logo'][0].path;
      hasFile = /(\.jpg|\.jpeg|\.png)$/.test(tmpPath);
      res.json(false);
      if (!hasFile) {
        return;
      }
      isJpeg = /(\.jpg|\.jpeg)$/.test(tmpPath);
      ext = 'jpg';
      if (!isJpeg) {
        ext = 'png';
      }
      newPath = __dirname + "/" + publicDir + "/img/performers-logo.";
      shell.rm(newPath + "png");
      shell.rm(newPath + "jpg");
      shell.mv(tmpPath, "" + newPath + ext);
      return res.json(true);
    });
  };

  uploadController.csv = function() {
    var ctrl, form, res;
    res = this.res;
    ctrl = this;
    form = new multiparty.Form();
    return form.parse(this.req, function(err, fields, files) {
      var bush, date, field, reportId, tmpPath, well;
      tmpPath = files['csv-file'][0].path;
      if (!/\.csv$/.test(tmpPath)) {
        res.json(false);
        return;
      }
      date = fields['date'][0];
      field = fields['field'][0];
      bush = fields['bush'][0];
      well = fields['well'][0];
      reportId = getReportId(date, field, bush, well);
      shell.mv(tmpPath, __dirname + "/" + publicDir + "/data/" + reportId + ".csv");
      return res.json(true);
    });
  };

  module.exports = uploadController;

}).call(this);
