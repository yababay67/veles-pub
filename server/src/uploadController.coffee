publicDir   = '../../../public'
getReportId = require "#{publicDir}/js/reportid.js"
locomotive  = require 'locomotive'
multiparty  = require 'multiparty'
shell       = require 'shelljs'

Controller  = locomotive.Controller

uploadController = new Controller

uploadController.logo = ->
    res  = @res
    ctrl = @
    form = new multiparty.Form()
    form.parse @req, (err, fields, files)->
        tmpPath  = files['performers-logo'][0].path
        hasFile  = /(\.jpg|\.jpeg|\.png)$/.test tmpPath
        res.json false; return unless hasFile
        isJpeg = /(\.jpg|\.jpeg)$/.test tmpPath
        ext = 'jpg'
        ext = 'png' unless isJpeg
        newPath = "#{__dirname}/#{publicDir}/img/performers-logo."
        shell.rm "#{newPath}png"
        shell.rm "#{newPath}jpg"
        shell.mv tmpPath, "#{newPath}#{ext}"
        res.json true

uploadController.csv = ->
    res  = @res
    ctrl = @
    form = new multiparty.Form()
    form.parse @req, (err, fields, files)->
        tmpPath  = files['csv-file'][0].path
        if not /\.csv$/.test tmpPath
            res.json false
            return
        date     = fields['date'][0]
        field    = fields['field'][0]
        bush     = fields['bush'][0]
        well     = fields['well'][0]
        reportId = getReportId date, field, bush, well
        shell.mv tmpPath, "#{__dirname}/#{publicDir}/data/#{reportId}.csv"
        res.json true

module.exports = uploadController
