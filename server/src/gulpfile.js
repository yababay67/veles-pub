var gulp    = require('gulp');
var coffee  = require('gulp-coffee');
var gutil   = require('gutil');

gulp.task('init', function() {
    gulp.src('*_*.coffee')
        .pipe(coffee())
        .pipe(gulp.dest('../config/initializers'))
})

gulp.task('app', function() {
    gulp.src('*Controller.coffee')
        .pipe(coffee())
        .pipe(gulp.dest('../app/controllers'))
})

gulp.task('default', ['init', 'app'])
