module.exports = function routes() {
  this.match('/json/upload/csv',  'upload#csv',  {via: 'POST'});
  this.match('/json/upload/logo', 'upload#logo', {via: 'POST'});
}
