#include <SoftwareSerial.h>
#include <ModbusSoftMaster.h>

ModbusSoftMaster ip320(2);
ModbusSoftMaster consumer(3);

void getIp320Register(uint8_t n){
  uint8_t result = ip320.readHoldingRegisters(n, 1);
  if(result == ip320.ku8MBSuccess){
    uint16_t wrd = ip320.getResponseBuffer(0);
    Serial.println(wrd);
  }
  else{
    Serial.print("IP320 error: ");
    Serial.println(result);
  }
}

void getConsumption(){
  union {
    uint16_t words[2];
    uint32_t counter;
  } registers;
  uint8_t result = consumer.readHoldingRegisters(0, 2);
  if(result == consumer.ku8MBSuccess){
    registers.words[0] = consumer.getResponseBuffer(0);
    registers.words[1] = consumer.getResponseBuffer(1);
    Serial.println(registers.counter);
  }
  else{
    Serial.print("Consumer error: ");
    Serial.println(result);
  }
}

void setup() {
  Serial.begin(9600);
  ip320.begin();
}

void loop(){
  if (Serial.available()) {
    delay(10);
    char c = Serial.read(); 
    switch(c){
      case 'c':
        getConsumption();
        break;
      case 'f':
        getIp320Register(0);
        break;
      case 'b':
        getIp320Register(1);
        break;
      case 'w':
        getIp320Register(2);
        break;
      case 't':
        getIp320Register(3);
        break;
      case 'p':
        getIp320Register(4);
        break;
      case 'd':
        getIp320Register(5);
        break;
      default:
        Serial.println("hz");
    }
  }
}

