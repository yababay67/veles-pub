#define COUNT_PIN 5
#define TICK 200

#include <RS485.h>
#include <TimerOne.h>

char message[maxMsgLen+3+1];
volatile unsigned long peaks = 0; 
volatile bool toCount = false;

void setup() {
  pinMode(COUNT_PIN, INPUT_PULLUP);
  Timer1.initialize(TICK);
  Timer1.attachInterrupt(count);
  Serial.begin(9600);
  RS485_Begin(28800);
}

void count() {
  bool state = digitalRead(COUNT_PIN);
  if(state && toCount) {peaks++; toCount = false;}
  if(!state) toCount = true;
}

void loop() {
  if(RS485_ReadMessage(fAvailable,fRead, message)){
    noInterrupts();
    if(message[0] != 'a') strcpy(message, "hz");
    else sprintf(message, "%lu", peaks);
    interrupts();
    RS485_SendMessage(message,fWrite,ENABLE_PIN);
  } 
}
